// This file is part of openZmeter.
//
// openZmeter is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// openZmeter is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with openZmeter.  If not, see <https://www.gnu.org/licenses/>
// ---------------------------------------------------------------------------------------------------------------------
#include "Telegram.h"
//----------------------------------------------------------------------------------------------------------------------
string                    Telegram::pToken       = "";
pthread_t                 Telegram::pThread      = 0;
pthread_mutex_t           Telegram::pMutex       = PTHREAD_MUTEX_INITIALIZER;
bool                      Telegram::pTerminate   = false;
uint32_t                  Telegram::pUsedWorkers = 0;
Headers                   Telegram::pHeaders     = {{"Accept", "application/json"}, {"Host", "api.telegram.org"}, {"Accept-Encoding", "gzip, deflate"}, {"Connection", "keep-alive"}};
list<Telegram::Worker_t*> Telegram::pFreeWorkers;
//----------------------------------------------------------------------------------------------------------------------
Telegram::Telegram() {
  HTTPServer::RegisterCallback("getTelegram", GetTelegramCallback);
  HTTPServer::RegisterCallback("setTelegram", SetTelegramCallback);
  Configure(CheckConfig(ConfigManager::ReadConfig("/Telegram")));
}
Telegram::~Telegram() {
  pthread_mutex_lock(&pMutex);
  pTerminate = true;
  if(pThread != 0) pthread_join(pThread, NULL);
  while(pUsedWorkers > 0) {
    pthread_mutex_unlock(&pMutex);
    usleep(100000);
    pthread_mutex_lock(&pMutex);
  }
  while(pFreeWorkers.size() > 0) {
    auto Tmp = pFreeWorkers.front();
    delete Tmp->Thread;
    delete Tmp->Client;
    pFreeWorkers.pop_front();
    free(Tmp);
  }
  pthread_mutex_unlock(&pMutex);
}
void Telegram::Configure(const json &config) {
  ConfigManager::WriteConfig("/Telegram", config);
  bool Enable = config["Enabled"];
  pthread_mutex_lock(&pMutex);
  pToken = config["Token"];
  if((Enable == true) && (pThread == 0)) {
    pTerminate = false;
    if(pthread_create(&pThread, NULL, Thread_Func, NULL) != 0) {
      pThread = 0;
      LOGFile::Error("%s:%d -> %s\n", __func__, __LINE__, strerror(errno));
    } else {
      pthread_setname_np(pThread, "TelegramServer");
    }
  } else if((Enable == false) && (pThread != 0)) {
    pTerminate = true;
    pthread_join(pThread, NULL);
    pThread = 0;
  }
  pthread_mutex_unlock(&pMutex);
}
json Telegram::CheckConfig(const json &config) {
  json ConfigOUT = json::object();
  ConfigOUT["Token"] = "";
  ConfigOUT["Enabled"] = false;
  if(config.is_object() == true) {
    if(config.contains("Token") && config["Token"].is_string()) ConfigOUT["Token"] = config["Token"];
    if(config.contains("Enabled") && config["Enabled"].is_boolean()) ConfigOUT["Enabled"] = config["Enabled"];
  }
  return ConfigOUT;
}
void Telegram::GetTelegramCallback(const json &params, Response &response) {
  if(params["UserName"] != "admin") return HTTPServer::FillResponse(response, "", "text/plain", HTTP_FORBIDDEN);
  json Config = ConfigManager::ReadConfig("/Telegram");
  HTTPServer::FillResponse(response, Config.dump(), "application/json", HTTP_OK);
}
void Telegram::SetTelegramCallback(const json &params, Response &response) {
  if(params["UserName"] != "admin") return HTTPServer::FillResponse(response, "", "text/plain", HTTP_FORBIDDEN);
  if((params.contains("Enabled") == false) || (params["Enabled"].is_boolean() == false)) return HTTPServer::FillResponse(response, "'Enabled' param is requiered", "text/plain", HTTP_BAD_REQUEST);
  if((params.contains("Token") == false) || (params["Token"].is_string() == false)) return HTTPServer::FillResponse(response, "'Token' param is requiered", "text/plain", HTTP_BAD_REQUEST);
  Configure(CheckConfig(params));
  HTTPServer::FillResponse(response, "", "application/json", HTTP_OK);
}
void *Telegram::Thread_Func(void *params) {
  LOGFile::Debug("Telegram BOT started\n");
  nice(10);
  int32_t    LastUpdate = 0;
  Headers    Headers    = {{"Accept", "application/json"}, {"Host", "api.telegram.org"}, {"Accept-Encoding", "gzip, deflate"}, {"Connection", "keep-alive"}};
  SSLClient *Client     = new SSLClient("api.telegram.org", 443);
  Client->set_keep_alive_max_count(100);
  Client->set_read_timeout(20, 0);
  Client->set_timeout_sec(30);
  Client->set_compress(true);
  while(pTerminate == false) {
    json Commands = nullptr;
    string URL   = "/bot" + pToken + "/getUpdates";
    Params Param = { { "timeout", to_string(TELEGRAM_POLLING_SEC) }, { "offset", to_string(LastUpdate + 1) }, { "limit", to_string(TELEGRAM_UPDATE_LIMIT) } };
    auto Response = Client->Post(URL.c_str(), Headers, Param);
    if(Response == nullptr) {
      usleep(100000);
      continue;
    }
    if(Response->status != 200) continue;
    json Params = json::parse(Response->body, NULL, false);
    if(Params.is_discarded() == true) continue;
    if((Params["ok"].is_boolean() == false) || (Params["ok"] == false)) continue;
    if(Params["result"].is_array() == false) continue;
    for(auto &Item : Params["result"]) {
      if(Item["update_id"].is_number()) LastUpdate = Item["update_id"];
      Worker_t *Tmp = NULL;
      pthread_mutex_lock(&pMutex);
      pUsedWorkers++;
      if(pFreeWorkers.size() > 0) {
        Tmp = pFreeWorkers.front();
        pFreeWorkers.pop_front();
      }
      pthread_mutex_unlock(&pMutex);
      if(Tmp == NULL) {
        Tmp = (Worker_t*)calloc(sizeof(Worker_t), 1);
        Tmp->Client = new SSLClient("api.telegram.org", 443);
        Tmp->Thread = new Worker("TelegramWorker", NULL, [](void *param1, void *param2) {
          Worker_t *Work = (Worker_t*)param2;
          HandleUpdate(Work->Client, Work->JSON);
          Work->Token = "";
          Work->JSON = nullptr;
          pthread_mutex_lock(&pMutex);
          Work->LastTime = Tools::GetTime();
          pFreeWorkers.push_front(Work);
          pUsedWorkers--;
          pthread_mutex_unlock(&pMutex);
        }, 15);
      }
      if(Tmp != NULL) {
        Tmp->Token = pToken;
        Tmp->JSON  = Item;
        Tmp->Thread->Run(Tmp);
      }
    }
  }
  LOGFile::Debug("Telegram BOT stopped\n");
  delete Client;
  return NULL;
}
void Telegram::AdjustWorkers() {
  pthread_mutex_lock(&pMutex);
  uint64_t Time = Tools::GetTime();
  while(pFreeWorkers.size() > 0) {
    auto Tmp = pFreeWorkers.back();
    if((Time - Tmp->LastTime) < 60000) break;
    delete Tmp->Thread;
    delete Tmp->Client;
    pFreeWorkers.pop_back();
    free(Tmp);
  }
  LOGFile::Debug("Adjusted Telegram workers pool to %d connections (plus %u in use)\n", pFreeWorkers.size(), pUsedWorkers);
  pthread_mutex_unlock(&pMutex);
}
bool Telegram::SendMessage(const json &msg) {
  string URL = "/bot" + pToken + "/sendMessage";
  SSLClient Client("api.telegram.org", 443);
  auto Response = Client.Post(URL.c_str(), pHeaders, msg.dump(), "application/json");
  return true; ///TODO: Comprobar valores devueltos
}
//----------------------------------------------------------------------------------------------------------------------
void Telegram::HandleUpdate(SSLClient *client, const json &JSON) {
  printf("%s\n", JSON.dump().c_str());
  try {
    if(JSON.contains("message")) {
      Database DB;
      int64_t ChatID = JSON["message"]["chat"]["id"];
      if(DB.ExecSentenceAsyncResult("SELECT username FROM users WHERE telegram = " + DB.Bind(to_string(ChatID))) == false) return;
      json Msg = json::object();
      Msg["chat_id"] = ChatID;
      if(DB.FecthRow() == false) {
        Msg["text"] = "Your user ID is not found, please check it is correctly set in user profile and try again\n\n*Your user ID is '" + to_string(ChatID) + "'*";
        Msg["parse_mode"] = "Markdown";
        SendMessage(Msg);
        return;
      }
      if(JSON["message"]["text"] == "/start") {
        Msg["text"] = "*Welcome* to *openZmeter*\n\nYou are ready to receive alarms and query your analyzers information.";
        Msg["parse_mode"] = "Markdown";
        SendMessage(Msg);
        //Envio teclado con analyzers
        return;

//      SendMessage(message.chat().id(), "Bienvenido "+message.chat().firstName()+ "👋👋👋\nA partir de este momento podrás disfrutar de"
//            + " todas las funcionalidades, y podrás recibir los eventos y alarmas en tiempo real", MAINMENU_KB);
      }
      Msg["text"] = "Unknown command";
      SendMessage(Msg);
    }
  } catch(...) {}
}



//void Telegram::HandleUpdate(SSLClient *client, const json &message) {
//  printf("%s\n", message.dump().c_str());
//

//
//        } else {
//            String command = message.text();
//            switch (command) {
//                case CDASHBOARD:
//                {
//                   sendDashboard(message.chat().id());
//                   break;
//                }
//                case CENERGY:
//                {
//                    sendMessage(message.chat().id(), "📅", ENERGY_KB);
//                    break;
//                }
//                case CEVENT:
//                {
//                    sendMessage(message.chat().id(), "🚩", EVENTS_KB);
//                    break;
//                }
//                case CALARM:
//                {
//                    sendMessage(message.chat().id(), "Coming soon", EVENTS_KB);
//                    break;
//                }
//                case CSYSTEM:
//                {
//                    int messageid = sendWaitGif(message.chat().id());
//                    OZMSystem ozmSys = new OZMSystem(true);
//                    sendMessageMarkDown(message.chat().id(), ozmSys.getSystemParameters(), MAINMENU_KB);
//                    sendDeleteWaitGif(message.chat().id(), messageid);
//                    ozmSys.closeSession();
//                    break;
//                }
//
//                case CREPORT:
//                {
//                    sendMessage(message.chat().id(), CREPORT, REPORT_KB);
//                    //updateIma(message.chat().id());
//                    sendMessage(36166036, "http://telegra.ph/oZm-daily-report-11-30", MAINMENU_KB);
//                    sendMessage(828068, "http://telegra.ph/oZm-daily-report-11-30", MAINMENU_KB);
//                    break;
//                }
//
//                case CBACKHOME:
//                {
//                    sendMessage(message.chat().id(), HOME, MAINMENU_KB);
//                    break;
//                }
//
//
//                case ETODAY:
//                {
//                    int messageid = sendWaitGif(message.chat().id());
//                    sendEnergyToday(message.chat().id());
//                    sendDeleteWaitGif(message.chat().id(), messageid);
//                    break;
//                }
//                case EYESTERDAY:
//                {
//                    int messageid = sendWaitGif(message.chat().id());
//                    sendEnergyYesterday(message.chat().id());
//                    sendDeleteWaitGif(message.chat().id(), messageid);
//                    break;
//                }
//                case EWEEK:
//                {
//                    int messageid = sendWaitGif(message.chat().id());
//                    sendEnergyThisWeek(message.chat().id());
//                    sendDeleteWaitGif(message.chat().id(), messageid);
//
//
//                    break;
//                }
//                case ELASTWEEK:
//                {
//                    int messageid = sendWaitGif(message.chat().id());
//                    sendEnergyLastWeek(message.chat().id());
//                    sendDeleteWaitGif(message.chat().id(), messageid);
//                    break;
//                }
//                case ETHISMONTH:
//                {
//                    int messageid = sendWaitGif(message.chat().id());
//                    sendEnergyThisMonth(message.chat().id());
//                    sendDeleteWaitGif(message.chat().id(), messageid);
//                    break;
//                }
//                case ELASTMONTH:
//                {
//                    int messageid = sendWaitGif(message.chat().id());
//                    sendEnergyLastMonth(message.chat().id());
//                    sendDeleteWaitGif(message.chat().id(), messageid);
//                    break;
//                }
//                case EYEAR:
//                {
//                    int messageid = sendWaitGif(message.chat().id());
//                    sendEnergyThisYear(message.chat().id());
//                    sendDeleteWaitGif(message.chat().id(), messageid);
//                    break;
//                }
//                case ELASTYEAR:
//                {
//                    int messageid = sendWaitGif(message.chat().id());
//                    sendEnergyLastYear(message.chat().id());
//                    sendDeleteWaitGif(message.chat().id(), messageid);
//                    break;
//                }
//
//                case EVLASTEVENTS:
//                {
//                    int messageid = sendWaitGif(message.chat().id());
//                    sendLastEvents(message.chat().id());
//                    sendDeleteWaitGif(message.chat().id(), messageid);
//                    break;
//                }
//                case EVTODAY:
//                {
//                    int messageid = sendWaitGif(message.chat().id());
//                    sendEventToday(message.chat().id());
//                    sendDeleteWaitGif(message.chat().id(), messageid);
//                    break;
//
//                }
//                case EVMONTH:
//                {
//                    int messageid = sendWaitGif(message.chat().id());
//                    sendEventMonth(message.chat().id());
//                    sendDeleteWaitGif(message.chat().id(), messageid);
//                    break;
//                }
//                case EVYEAR:
//                {
//                    int messageid = sendWaitGif(message.chat().id());
//                    sendEventYear(message.chat().id());
//                    sendDeleteWaitGif(message.chat().id(), messageid);
//                    break;
//                }
//
//                default: {
//                    sendMessage(message.chat().id(), "Comando Incorrecto", MAINMENU_KB);
//                    System.out.println("No action");
//                }
//
//            }
//
//        }
//





//  json Msg = json::object();
//  Msg["chat_id"] = 242623189;
//  Msg["text"] = "Soy un mensaje!";
//  Msg["reply_markup"]
//  SendMessage(Msg);
//

//}